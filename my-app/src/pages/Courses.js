import React, { Fragment, useContext } from 'react';
import { Redirect } from 'react-router-dom';
import UserContext from '../UserContext';
import { Table, Button } from 'react-bootstrap';
import Course from '../components/Course';
import coursesData from '../data/courses';

export default function Courses(){
	//use the UserContext and destructure it to access the user state defined
	//in App component
	const { user } = useContext(UserContext);

	//create multiple Course component corresponding to the content of courseData
	const courses = coursesData.map(course => {
		if(course.onOffer){
			return (
				<Course key={course.id} course={course} />
			);
		} else {
			return null;
		}
	})
	
	//table rows to be rendered in a bootstrap table when an admin is logged in
	const coursesRows = coursesData.map(course => {
		return (
			<tr key={course.id}>
				<td>{course.id}</td>
				<td>{course.name}</td>
				<td>{course.description}</td>
				<td>Php {course.price}</td>
				<td>{course.onOffer ? 'open':'closed'}</td>
				<td>{course.start_date}</td>
				<td>{course.end_date}</td>
				<td>
					<Button variant="warning">Update</Button>
					<Button variant="danger">Disabled</Button>
				</td>
			</tr>
		);
	})

	return (
		user.isAdmin === true ?
		<Fragment>
			<h1>Courses Dashboard</h1>
			<Table>
				<thead>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Status</th>
						<th>Start Date</th>
						<th>End Date</th>
						<th>Actions Taken</th>
					</tr>
				</thead>
				<tbody>
					{coursesRows}
				</tbody>
			</Table>
		</Fragment>
		:
		<Fragment>
			{courses}
		</Fragment>

	)
}