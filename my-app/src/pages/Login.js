import React, { useState, useEffect, useContext} from 'react';
import { Redirect, Router } from 'react-router-dom';
import { Form, Button } from 'react-bootstrap';
import UserContext from '../UserContext';
import usersData from '../data/users';

export default function Login() {
	const { user, setUser} = useContext(UserContext);
    const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(true);

    //state for redirection
    const [ willRedirect, setWillRedirect ] = useState(false);

    //use a useEffect hook that will run whenever any of the user details 
    //have to changed to check for user authentication
    useEffect(() => {
        console.log(`User with an email: ${email} is an admin: ${user.isAdmin}`);
    }, [user.isAdmim, user.email])

	function authenticate(e) {
		e.preventDefault();
        

        //Authentication based on imported users data
        const match = usersData.find( user => {
            return (user.email === email && user.password === password);
        })

        if(match){
            //set the details of the authenticated user in the local storage
            localStorage.setItem('email', email);
            //this will be converted to be a string once it is set in the localStorage
            localStorage.setItem('isAdmin', match.isAdmin);

            //set the global user state to have properties obtained from local Storage
            setUser({
                email: localStorage.getItem('email'),
                isAdmim: match.isAdmin
            });
            setWillRedirect(true);
        } else {
            alert('Authentication failed, no match found.')
            
        }

        //clear input fields after submission
		setEmail('');
		setPassword('');

        //set the email of the authentiated user in the local storage
		//alert(`${email} has been verified! Welcome back!`);
      
    
	}
	// useEffect(() => {

    //     // Validation to enable submit button when all fields are populated
    //     if(email !== '' && password !== ''){
    //         setIsActive(true);
    //     }else{
    //         setIsActive(false);
    //     }

    // }, [email, password]);

    

    //use conditional rendering to redirect to /courses if willRedirect state
    //is true
    return (
        willRedirect === true ?
            <Redirect to='/courses' />
            :
            <Form onSubmit={(e) => authenticate(e)}>
                <Form.Group controlId="userEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control 
                        type="email" 
                        placeholder="Enter email"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group controlId="password">
                    <Form.Label>Password</Form.Label>
                    <Form.Control 
                        type="password" 
                        placeholder="Password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        required
                    />
                </Form.Group>

                {isActive ? 
                    <Button variant="primary" type="submit" id="submitBtn">
                        Submit
                    </Button>
                    : 
                    <Button variant="danger" type="submit" id="submitBtn" disabled>
                        Submit
                    </Button>
                }
               
            </Form>
            
    )
    
}